require 'spec_helper'
require 'bughunting/check/carrier'
require 'bughunting/check/task'
require 'bughunting/common/local_execute'
require "bughunting/server/config"
require 'tmpdir'

RSpec.describe RemoteCarrier do

  class LocalExecuteClass
    include LocalExecute
  end

  let(:server_config) { ServerConfig.get_defaults }
  let(:script_names) { ['script.sh', 'script2.sh'] }
  let(:warm_up_fixture_path) { File.join fixtures_path, 'warm_up' }
  let(:local_execute) { LocalExecuteClass.new }
  let(:warm_up_task) { Task.new("#{fixtures_path}/cache/warm_up") }
  let(:file_content) { 'foo and bar' }
  let(:newfile_patch) {
    script_names.map do |script_name|
      <<~EOF
--- /dev/null
+++ #{script_name}
@@ -0,0 +1,1 @@
+#{file_content}
      EOF
    end
  }
  let(:patch_from_diffs) {
    script_names.map do |script_name|
      # Strip gets rid of newline that would get duplicated otherwise
      <<~EOF
--- #{fixtures_path}/cache/warm_up/source/#{script_name}
+++ #{script_name}
@@ -1 +1 @@
-#{file_content}
+#{script_name}
      EOF
    end
  }

  let(:carrier_with_newfile_patch) { RemoteCarrier.new(server_config, nil, warm_up_task, newfile_patch) }
  let(:carrier_with_patch_from_diffs) { RemoteCarrier.new(server_config, nil, warm_up_task, patch_from_diffs) }
  let(:jail_dir) { Dir.mktmpdir('bughunting') }
  let(:jail_dir_content) { Dir.children jail_dir }

  describe '#get' do
    context 'when apply_patch is false' do
      it 'uses ssh to copy the files' do
        command = carrier_with_newfile_patch.send :command, "#{fixtures_path}", ["#{fixtures_path}/warm_up"]
        expect(carrier_with_newfile_patch).to receive(:system).with(*command)
        carrier_with_newfile_patch.get fixtures_path, [warm_up_fixture_path]
      end
    end

    context 'when apply_patch is true' do
      before do
        server_config['apply_patch'] = true
      end

      after do
        FileUtils.remove_entry jail_dir
      end

      let(:patched_files) { script_names.map { |script_name| File.join(jail_dir, script_name) } }

      context 'with no files present in target directory' do
        context 'when patch is done against /dev/null' do
          it 'applies the patch' do
            carrier_with_newfile_patch.get jail_dir, nil

            patched_files.each do |patched_file|
              expect(File.read(patched_file)).to match file_content
            end
          end
        end
      end

      context 'with files already present in target directory' do
        context 'when patch is done against /dev/null' do
          it 'overwrites the files with patch contents' do
            4.times do |number|
              File.write(File.join(jail_dir, "file#{number}"), number)
            end

            carrier_with_newfile_patch.get jail_dir, nil

            expect(jail_dir_content.count).to eq 2

            patched_files.each do |patched_file|
              expect(File.read(patched_file)).to match file_content
            end
          end
        end

        context 'when patch is done against cache' do
          before do
            warm_up_task.config['copy_task_on_server'] = true
            server_config['apply_patch'] = true

            patched_files.each do |patched_file|
              File.write(patched_file, file_content + $/)
            end
          end

          it 'applies the patch on existing files' do
            carrier_with_patch_from_diffs.get jail_dir, nil

            expect(jail_dir_content.count).to eq 2
            patched_files.each do |patched_file|
              expect(File.read(patched_file)).to match File.basename(patched_file)
            end
          end
        end
      end
    end
  end

  describe '#patch' do
    it 'merges patches into one string' do
      expect(carrier_with_newfile_patch.patch).to eq newfile_patch.join $/
    end

    context 'when skip_check is false' do
      it 'validates the patch' do
        expect(carrier_with_newfile_patch).to receive(:validate_patch).with(newfile_patch).once
        2.times { carrier_with_newfile_patch.patch }
      end
    end

    context 'when skip_check is true' do
      it 'does not validate the patch' do
        expect(carrier_with_newfile_patch).not_to receive(:validate_patch).with(newfile_patch)
        carrier_with_newfile_patch.patch(true)
      end

      describe '#patch' do
        it 'merges patches into one string' do
          expect(carrier_with_newfile_patch.patch).to eq newfile_patch.join $/
        end
      end

      context 'when skip_check is false' do
        it 'validates the patch' do
          expect(carrier_with_newfile_patch).to receive(:validate_patch).with(newfile_patch)
          carrier_with_newfile_patch.patch
        end
      end

      context 'when skip_check is true' do
        it 'does not validate the patch' do
          expect(carrier_with_newfile_patch).not_to receive(:validate_patch).with(newfile_patch)
          carrier_with_newfile_patch.patch(true)
        end
      end
    end
  end
end

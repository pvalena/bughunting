require 'bughunting/check/task_config'

RSpec.describe TaskConfig do
  describe '#get_defaults' do
    context 'copy_task_on_server' do
      it 'is false' do
        expect(TaskConfig.get_defaults["copy_task_on_server"]).to be false
      end
    end
  end
end

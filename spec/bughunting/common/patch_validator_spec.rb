require 'spec_helper'
require 'bughunting/common/patch_validator'

RSpec.describe PatchValidator do
  class DummyTestClass
    include PatchValidator
  end

  let(:dummy_class) { DummyTestClass.new }

  describe '#validate_present' do
    context 'with a line' do
      let(:line) { '+++ foobar 123456' }

      context 'with matching expression' do
        let (:expression) { '\+\+\+ ' }

        it 'returns the match' do
          expect(dummy_class.validate_present(line, expression)).to eq 'foobar'
        end

        context 'when valid block is given' do
          let(:block) { Proc.new{ |match| match.is_a?(String) } }

          it 'returns the match' do
            expect(dummy_class.validate_present(line, expression, &block)).to eq 'foobar'
          end
        end

        context 'when failing block is given' do
          let(:block) { Proc.new{ |match| match.is_a?(Hash) } }

          it 'raises exception when block returns false' do
            expect {dummy_class.validate_present(line, expression, &block)}.to raise_error(RuntimeError, 'Patch validation failed.')
          end
        end

        context 'with non-matching expression' do
          let(:expression) { '---' }

          it 'raises exception with custom message' do
            expect {dummy_class.validate_present(line, expression)}.to raise_error(RuntimeError, 'Patch validation failed.')
          end
        end
      end
    end
  end

  describe '#validate_absent' do
    context 'with lines' do
      let(:lines_array) { "+++ foobar 123456\nabc\nbac".each_line }

      context 'with no match' do
        let(:expression) { '---' }

        it 'passes' do
          expect(dummy_class.validate_absent(lines_array, expression)).to be nil
        end
      end

      context 'with a match' do
        let(:expression) { '.*123456.*' }

        it 'raises exception with custom message' do
          expect{dummy_class.validate_absent(lines_array, /#{expression}/)}.to raise_error(RuntimeError, 'Patch validation failed.')
        end
      end
    end
  end

  describe '#validate_diff' do
    context 'with valid diff' do
      let(:diff) {
<<-EOT
--- a/b/c 123
+++ b 123
foo
EOT
      }

      it 'returns array of the target file path' do
        expect(dummy_class.validate_diff(diff)).to eq ['b']
      end

      context 'with given block' do
        it 'accepts block' do
          block = Proc.new { |match| match.is_a?(String) }
          expect(dummy_class.validate_diff(diff, &block)).to eq ['b']
        end

        context 'when block fails validation' do
          it 'raises exception with custom message' do
            block = Proc.new { |match| match.is_a?(Hash) }

            expect{dummy_class.validate_diff(diff, &block)}.to raise_error(RuntimeError, 'Patch validation failed.')
          end
        end
      end
    end

    context 'with diff that has invalid first line' do
      let(:diff) {
<<-EOT
a+a v
+++ a
a
EOT
      }

      # The `patch` command does not care about the first line at all when applying
      it 'passes' do
        expect(dummy_class.validate_diff(diff)).to eq ['a']
      end
    end

    context 'with diff that has invalid second line' do
      let(:diff) {
<<-EOT
--- a
a+a v
a
EOT
      }

      it 'raises exception with custom message' do
        expect{dummy_class.validate_diff(diff)}.to raise_error(RuntimeError, 'Patch validation failed.')
      end
    end
  end

  describe '#validate_patch' do
    context 'with patch' do
      let(:patch) {
        [
          "--- foo 123\n+++ foobar 123456\nabc\nbac",
          "--- foobar 123\n+++ foo 123456\ncba\ncaa",
        ]
      }
      it 'validates each diff' do
        expect(dummy_class.validate_patch(patch)).to eq [ ['foobar'], ['foo'] ]
      end

      context 'with block' do
        it 'passes it for validation' do
          block = Proc.new { |match| match.include?('foo') }

          expect(dummy_class.validate_patch(patch, &block)).to eq [ ['foobar'], ['foo'] ]
        end

        context 'when block fails validation' do
          it 'raises exception with custom message' do
            block = Proc.new { |match| match.is_a?(Integer) }

            expect{dummy_class.validate_patch(patch, &block)}.to raise_error(RuntimeError, 'Patch validation failed.')
          end
        end
      end
    end
  end

  describe 'validate_fail' do
    context 'with no defined message' do
      it 'raises exception with custom message' do
        expect{dummy_class.validate_fail}.to raise_error(RuntimeError, 'Patch validation failed.')
      end
    end

    context 'when class has defined @fail_message' do
      class TestClass
        include PatchValidator

        def initialize
          @fail_message = 'Hello world.'
        end
      end

      it 'uses @fail_message as exception message' do
        test_class = TestClass.new

        expect{test_class.validate_fail}.to raise_error(RuntimeError, 'Hello world.')
      end
    end
  end
end

require 'bughunting/client/controller'
require 'bughunting/common/patch_validator'
require 'tmpdir'

RSpec.describe ClientController do
  let(:task) { Task.new("#{fixtures_path}/cache/warm_up/") }
  # Configuration for client controller, overrides ClientConfig
  let(:client_config) {
    {
      "server"=>
      {
        "host"=>nil,
        "port"=>2230
      },
      "paths"=>
      {
        "cache_dir"=> "#{fixtures_path}/cache",
        "sources_dir"=> fixtures_path
      }
    }
  }

  let(:client_controller) { ClientController.new(nil, 'warm_up') }

  describe '#create_patch' do
    let(:patch) { client_controller.create_patch }

    before do
      allow(ClientConfig).to receive(:load_file).with(nil).and_return(client_config)
      allow(Task).to receive(:new).and_return(task)
    end

    context 'when copy_task_on_server is false' do
      it 'creates a patch against /dev/null' do
        expect(patch.first).to include(script_diff)
        expect(patch_validator.validate_patch(patch)).to eq [['script.sh'], ['script2.sh']]
      end
    end

    context 'when copy_task_on_server is true' do
      before do
        task.config['check_files'] = ['script.sh']
        task.config['copy_task_on_server'] = true
      end

      it 'creates patch against original sources' do
        expect(patch.first).to include(script_diff_against_cache)
        expect(patch_validator.validate_patch(patch)).to eq [['script.sh']]
      end
    end

    context 'task defines multiple files' do
      it 'returns array of diffs' do
        expect(patch.count).to eq 2
        expect(patch_validator.validate_patch(patch)).to eq [['script.sh'], ['script2.sh']]
      end
    end
  end

  describe '#clean' do
    let(:tasks_dir) { Dir.mktmpdir('bughunting') }
    let(:warm_up_dir) { File.join tasks_dir, task.config['name'] }
    let(:source_files) { Dir.children(File.join(fixtures_path, 'cache', 'warm_up', 'source')).sort }
    let(:preexisting_file_path) { File.join warm_up_dir, 'foo.file' }

    before do
      # Let's silence the output to not have unnecessary clutter in the output
      allow($stderr).to receive(:puts)
      allow(ClientConfig).to receive(:load_file).with(nil).and_return(client_config)
      client_config['paths']['sources_dir'] = tasks_dir
      Dir.mkdir(warm_up_dir)
    end

    after do
      FileUtils.rm_rf tasks_dir
    end

    it 'outputs what task is being cleaned' do
      expect do
        Dir.chdir(warm_up_dir) do
          client_controller.clean
        end
      end.to output("Cleaning task '#{task.config['name']}'\n").to_stderr
    end

    context 'when task path exists' do
      before do
        File.write(preexisting_file_path, '1234')
        Dir.chdir(warm_up_dir) do
          client_controller.clean
        end
      end

      it 'deletes the contents' do
        expect(File).not_to exist preexisting_file_path
      end

      it 'copies the source files from cache_dir' do
        expect(Dir.children(warm_up_dir).sort).to eq source_files
      end
    end

    context 'when there are edited source files' do
      before do
        source_files.each do |script_name|
          script_path = File.join warm_up_dir, script_name
          File.write(script_path, "foobar #{File.basename(script_path)}")
        end

        client_controller.clean
      end

      it 'renews the contents of the files' do
        Dir.children(warm_up_dir).sort.each_with_index do |script_name, index|
          script_path = File.join warm_up_dir, script_name
          source_file_path = File.join task.dir, 'source', source_files[index]
          expect(FileUtils.compare_file(script_path, source_file_path)).to be true
        end
      end
    end

    context 'when task path does not exist' do
      before do
        FileUtils.remove_dir(warm_up_dir)
        Dir.chdir(tasks_dir) do
          client_controller.clean
        end
      end

      it 'copies the source files from cache_dir' do
        expect(Dir.children(warm_up_dir).sort).to eq source_files
      end
    end
  end

  describe '#serverclean' do
    before do
      allow(ClientConfig).to receive(:load_file).with(nil).and_return(client_config)
    end

    context 'when informing user' do
      it 'outputs to stderr what task is being cleand' do
        allow_any_instance_of(XMLRPC::Client).to receive(:call).and_return(nil)
        expect { client_controller.serverclean }.to output("Cleaning task #{task.config['name']}\n").to_stderr
      end
    end

    context 'with task' do
      before do
        # Let's silence the output to not have unnecessary clutter in the output
        allow($stderr).to receive(:puts)
      end

      it 'sends XMLRPC clean request to server with task name' do
        expect_any_instance_of(XMLRPC::Client).to receive(:call).with('bughunting.clean', task.config['name'])
        client_controller.serverclean
      end
    end
  end
end

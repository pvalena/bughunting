FROM registry.fedoraproject.org/f29/ruby

ENV APP_PATH="/opt/bughunting"

COPY . ${APP_PATH}

USER 0

RUN cd ${APP_PATH} && \
    dnf install -y 'dnf-command(builddep)' rpm-build xz && \
    dnf builddep -y ./bughunting.spec && \
    ./container_setup.sh && \
    dnf install -y ./rpm-build-dir/noarch/* && \
    dnf clean all

USER 1001

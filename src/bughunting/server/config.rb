#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/common/config"

class ServerConfig < BaseConfig
  def self.get_defaults
    {
      "listen" => {
        "ip" => "0.0.0.0",
        "port" => 2230,
      },
      "limits" => {
        "cputime_seconds" => 5,
        "memory_megabytes" => 32,
        "process_count" => 32,
        "stack_size" => 4096
      },
      "apply_patch" => false,
      "clients" => {
        "user" => "test",
        "identity_file" => "/etc/bughunting/remote_id_rsa",
        "sources_dir" => "sources",
        "cache_dir" => "/var/lib/bughunting/cache",
      },
      "paths" => {
        "tasks" => "/usr/share/bughunting/tasks",
        "archive" => "/var/run/bughunting/archive",
      },
    }.clone
  end
end

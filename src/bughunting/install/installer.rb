#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/common/local_execute"

# TODO: needs rewrite!

class Installer

  include LocalExecute

  attr_reader :error

  def initialize(host, user, identity_file, sources_dir, cache_dir, debug=false)
    @host = host
    @user = user
    @identity_file = identity_file
    @sources_dir = sources_dir
    @cache_dir = cache_dir
    @debug = debug
    @error = false
  end

  def try_ssh
    unless remote_execute( "echo " )
      @error = true
      STDERR.puts @host + " ERROR: ssh connection failed."
    end
  end

  def clean_full
    remote_execute "rm -rf ~/#{@sources_dir}; mkdir -p ~/#{@sources_dir}"
    remote_execute "rm -rf #{@cache_dir}; mkdir -m 0700 -p #{@cache_dir}", true
  end

  def push_sources(tasks)
    tasks.each do |task_name, task|
      to = "~#{@user}/#{@sources_dir}/#{task_name}"
      if task.symlinked_source
        from = task.source_dir.split("/")[-2]
        unless remote_execute "ln -sf #{from} #{to}"
          @error = true
          STDERR.puts @host + " ERROR: cannot symlink tasks."
          break;
        end
      else
        from = task.source_dir
        unless ssh_copy_to(from + '/', to)
          @error = true
          STDERR.puts @host + " ERROR: cannot copy tasks."
          break
        end
      end
    end
  end

  def run_install(tasks)
    tasks.each do |task_name, task|
      next if task.config["dependencies"].nil? || task.config["dependencies"].empty?
      unless task.config["dependencies"].kind_of?(Array)
        @error = true
        STDERR.puts @host + " ERROR: #{task_name}->dependencies is not array"
	next
      end
      dependencies = Shellwords.join(task.config["dependencies"])

      unless remote_execute( "dnf install -y #{dependencies}", true)
        @error = true
        STDERR.puts @host + " ERROR: installation of dependencies failed."
        break
      end
    end
  end

  def run_setup(tasks)
    tasks.each do |task_name, task|
      next if task.symlinked_source

      setup_commands = task.config["setup_commands"]
      next if setup_commands.nil? or setup_commands.empty?

      commands = []
      commands << "cd ~#{@user}/#{@sources_dir}/#{task_name}"
      commands += setup_commands

      unless remote_execute commands.join(";")
        @error = true
        STDERR.puts @host + " ERROR: error."
      end
    end
  end

  def create_cache(tasks)
    copy_sources = []
    tasks.each do |task_name, task|
      copy_sources << "~#{@user}/#{@sources_dir}/#{task_name}"
    end
    remote_execute (["cp", "-ar"] + copy_sources + [ @cache_dir ]).join(" "), true
  end

  private

  def remote_execute(command, superuser=false)
    local_execute [
      "/usr/bin/ssh",
      "-o", "BatchMode=yes",
      "-o", "StrictHostKeyChecking=no",
      "-i", @identity_file,
      "-l", superuser ? "root" : @user,
      @host,
      "/bin/sh -xc '#{command}'"
    ], @debug
  end

  def ssh_copy_to(from, to)
    local_execute [
      "/usr/bin/rsync",
      "-a",
      "-e", "ssh -o BatchMode=yes -o GSSAPIAuthentication=no -o StrictHostKeyChecking=no -i \"#{@identity_file}\"",
      from,
      "#{@user}@#{@host}:#{to}"
    ], @debug
  end
end

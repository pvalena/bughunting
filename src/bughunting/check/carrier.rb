#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "fileutils"
require "bughunting/common/patch_validator"
require "bughunting/common/local_execute"

class CarrierFactory
  def initialize(remote_sources_root)
    @remote_sources_root = remote_sources_root
  end

  def build_local(source_dir)
    LocalCarrier.new source_dir
  end

  def build_remote(user, host, identity_file, sources_dir, task_name, patch)
    RemoteCarrier.new user, host, identity_file, @remote_sources_root, task_name, patch
  end
end

class LocalCarrier
  def initialize(source_dir)
    @source_dir = source_dir
  end

  def get(target_dir, source_files)
    unless File.directory? target_dir
      raise "Target dir is not a directory"
    end

    source_files.each do |filename|
      filename_base = File.basename filename
      full_source = File.join @source_dir, filename
      full_target = File.join target_dir, filename_base

      FileUtils.cp_r full_source, full_target
    end
  end
end

class RemoteCarrier

  include PatchValidator, LocalExecute

  def initialize(server_config, host, task, unvalidated_patch)
    @user = server_config['clients']['user']
    @identity_file = server_config['clients']['identity_file']
    @sources_root = server_config['clients']['sources_dir']
    @apply_patch = server_config['apply_patch']
    @host = host
    @task = task
    @patch = unvalidated_patch
    @patch_validated = false
  end

  def get(target_dir, source_files)
    if @apply_patch
      FileUtils.rm_rf File.join(target_dir, '.')  unless @task.config['copy_task_on_server']
      local_execute('patch -f -F 0', input: patch, work_dir: target_dir)
    else
      system *command(target_dir, source_files)
    end
  end

  def patch(skip_check = false)
    unless @patch_validated || skip_check
      validate_patch(@patch) { |match| @task.config['check_files'].include?(match) }
      @patch_validated = true
    end
    @patch.join $/
  end

  private

  def command(target_dir, source_files)
    [
      "/usr/bin/scp",
      "-pr", # preserve attributes, recursive
      "-o", "BatchMode=yes",
      "-o", "StrictHostKeyChecking=no",
      "-i", @identity_file,
      sources(source_files),
      target_dir
    ]
  end

  def sources(source_files)
    sources = "#{@user}@#{@host}:#{@sources_root}/#{@task_name}/"
    if source_files.size == 1
      sources += source_files[0]
    else
      sources += "{%s}" % source_files.join(",")
    end
    sources
  end
end

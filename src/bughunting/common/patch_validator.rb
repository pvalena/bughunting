module PatchValidator
  def validate_fail
    raise (@fail_message || 'Patch validation failed.')
  end

  def validate_patch(patch, &block)
    patch.map do |diff|
      validate_diff diff, &block
    end
  end

  def validate_diff(diff, &block)
    lines = diff.split $/
    validate_absent lines[2...], '--- '
    validate_absent lines[2...], '\+\+\+ '

    [
      validate_present(lines[1], '\+\+\+ ', &block)
    ]
  end

  def validate_present(line, expression, &block)
    match = line.scan(/^#{expression}(\S*)\s*.*$/).flatten
    validate_fail unless match.one?
    match = match.first
    if block_given?
      validate_fail unless yield match
    end
    match
  end

  def validate_absent(lines, expression)
    validate_fail unless lines.none? { |line| line.start_with?(expression) }
  end
end

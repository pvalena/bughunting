README
============

Instructions how to configure client:

- create user (default name is test - server.yml)

- install bughunting and bughunting-client packages
    root@client> yum install bughunting bughunting-client

- always consider what dependencies should be installed via ks file, what should
  be client-required and what should be tasks-required (specfile in task
  repository)

- make *sure* everything from DEPLIST (git with tasks) is installed

- make sure all additional dependencies are installed.  Basically, we tested
  only on machines which had at least 'git' (and deps) installed, and also
  'bughunting-server' (and deps).  This really requires some amount of packages
  and absence of such package may break some task.

- distribute admin's public key into /root/.ssh/authorized_keys and
  ~/.ssh/authorized_keys
    root@server> ssh-keygen
    root@server> ssh-copy-id root@client
    root@server> ssh-copy-id test@client
    root@server> cat /etc/bughunting/remote_id_rsa.pub | \
                 ssh client 'cat>>/root/.ssh/authorized_keys'
    root@server> cat /etc/bughunting/remote_id_rsa.pub | \
                 ssh client 'cat>>/home/test/.ssh/authorized_keys'


Instructions how to configure server:

- install bughunting, bughunting-server and bughunting-web packages
    root@server> yum install bughunting bughunting-server bughunting-web

- install tasks from task repository into /var/lib/bughunting/tasks

    your_box> git clone https://gitlab.cee.redhat.com/bughunting/bhtasks-2017.git
    your_box> cd bhtasks-2017
    your_box> make rpms

    # This ensures that all the dependencies are also installed on the server,
    # to run check scripts successfully.  Note that if you add some dependancy
    # into some task.yml on-the-fly (without rebuilding bughunting-task)
    # package, you have to install the dependency aslo on the server manually.
    root@server> dnf -y install bughunting-tasks-2017-1.fc25.x86_64.rpm


- execute huntinstall for every client machine (copies all sources, installs
  package dependencies and runs setup commands).  Configure 'players' section in
  /etc/bughunting/web.yml, and run:

    root@server> huntinstall all # installs on all boxes in parallel

  Or manually with:

    root@server> huntinstall clienthostname1
    root@server> huntinstall clienthostname2
    ...


Instructions how to run the server:

- start the bughunting server:
    root@server> /usr/bin/huntserver &

- start the web server:
    root@server> /usr/share/bughunting/web/web.rb &

- show the scoreboard on http://127.0.0.1/scoreboard/
    
- start the game:
    root@server> date +%s > /var/lib/bughunting/running

- stop the game:
    root@server> mv /var/lib/bughunting/* 


Instructions how to run the client:
- register and read the task description at http://server/

- solve the task:
    test@client> cd ~/sources/task
    test@client> fix the code
    test@client> hunt



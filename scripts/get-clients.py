#!/usr/bin/python

import yaml
try:
    with open('/etc/bughunting/web.yml', 'r') as f:
        doc = yaml.load(f)
        print(" ".join([v for k, v in doc['players'].items() if k != '127.0.0.1']))
except IOError:
    print("westford stuttgart raleigh pune cork farnborough mountainview munich neuchatel raanana brisbane beijing")

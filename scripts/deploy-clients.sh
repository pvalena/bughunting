#!/bin/bash

if [ $# -ne 1 ] ; then
  echo "Usage `basename $0` serverip"
  exit 1
fi

export SERVER_IP="$1"
shift

#echo "Installing packages on the server..."
#yum -y install /root/bughunting{,-server,-web,-tasks}-20*rpm

deploy_client() {
  client_hostname="$1"
  
  # where to log output
  logfile="$client_hostname-`date +%s`.log"
  touch $logfile

  echo "getting ip of $client_hostname..."
  client_ip=`getent hosts $client_hostname|cut -d' ' -f1`

  echo "copy key to root..."
  ssh-copy-id -i /etc/bughunting/remote_id_rsa root@$client_ip &>>$logfile

  echo "creating test user..."
  ssh -i /etc/bughunting/remote_id_rsa root@$client_ip 'useradd test -p opensourcerulez' &>>$logfile

  echo "copy key to test..."
  cat /etc/bughunting/remote_id_rsa.pub | \
                 ssh -i /etc/bughunting/remote_id_rsa root@$client_ip 'mkdir -p /home/test/.ssh/ ; touch /home/test/.ssh/authorized_keys ; cat>>/home/test/.ssh/authorized_keys' &>>$logfile

  #echo "copying packages..."
  #scp -i /etc/bughunting/remote_id_rsa /root/bughunting{,-client}-20*rpm root@$client_ip: &>>$logfile

  #echo "installing packages..."
  #ssh -i /etc/bughunting/remote_id_rsa root@$client_ip 'yum -y install vim sudo /root/bughunting{,-client}-20*rpm' &>>$logfile

  echo "changing client conf..."
  ssh -i /etc/bughunting/remote_id_rsa root@$client_ip "sed -ie 's/host: .*$/host: $SERVER_IP/g' /etc/bughunting/client.yml" &>>$logfile

  #echo "installing bughunting client..."
  #huntinstall $client_hostname &>>$logfile &


  #echo "adding $client_hostname to web.yml..."
  #echo "    $client_ip: $client_hostname">>/etc/bughunting/web.yml
  
  
}

# we keep forked pids here
subprocesses=$#

get_clients_script="`dirname "${BASH_SOURCE[0]}"`/get-clients.py"
for client in `$get_clients_script` ; do
  deploy_client $client
  shift
done

# wait childern finish
for p in `seq $subprocesses` ; do
  echo "Waiting for process #$p..."
  wait
done

echo "Done."

#!/bin/sh

SSH_OPTS="-o BatchMode=yes -o StrictHostKeyChecking=no -i /etc/bughunting/remote_id_rsa"

get_clients_script="`dirname "${BASH_SOURCE[0]}"`/get-clients.py"

clients="`$get_clients_script`"

for machine in $clients ; do
  echo \# $machine

  ssh $SSH_OPTS root@$machine << END
    date --set="`date`"
    pkill -9 -u test
    rm -rf ~test
    cp -ar /etc/skel ~test
    mkdir -p ~test/.ssh
    cp ~root/.ssh/authorized_keys ~test/.ssh/authorized_keys
    chmod -R u=rwX,g=,o= ~test/.ssh
    chown -R test:test ~test
    echo "test" | passwd --stdin test
    usermod -L test
END

  huntinstall $machine &>$machine.log &
done

# wait for huntinstall call finishes on all clients
wait

for machine in $clients ; do
  echo Unlocking \# $machine
  ssh $SSH_OPTS root@$machine 'usermod -U test'
done

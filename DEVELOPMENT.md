# Development

## Architecture
Bughunting server currently uses SCP / SSH calls, with root privileges, to access files on clients, or to make other changes. We currently aim to change that.

### Tasks
* Instead of copying sources from client add an option into task to send diffs to server. !34
* Add build_commands option to define commands that need to be run to build the task. !36
  * Display the commands in the web UI.
* After copying files, execute the rest of the jail under the user submitted the task.
* Send a patch of sources for check instead of copying the files over SSH !39
  * send the whole file as a patch if copy_task_on_server option is false. !39
  * create a diff from sources file if copy_task_on_server option is true. !39
* Apply the diff or create a new file from the diff.
* save the patch into a file for archiving
* diff validation: check for valid files. !42
* Create 'apply_patch' option in server config to influence the flow of carrier get method
  * copy over SCP as it was done so far if the option is false
  * apply the patch if the option is true

#!/usr/bin/ruby

require "test/unit"
require "bughunting/server/users"

class TestServerUsers < Test::Unit::TestCase
  def test_empty
    users = Users.new
    assert_nil users.get_free "127.0.0.1"
  end

  def test_full
    users = Users.new ["user1"]

    assert_not_nil users.get_free "1.1.1.1"
    assert_nil users.get_free "2.2.2.2"
    users.return "1.1.1.1"

    assert_not_nil users.get_free "2.2.2.2"
  end

  def test_running
    users = Users.new ["user1", "user2"]

    assert_not_nil users.get_free "1.1.1.1"
    assert_nil users.get_free "1.1.1.1"
    users.return "1.1.1.1"
    assert_not_nil users.get_free "1.1.1.1"
  end
end

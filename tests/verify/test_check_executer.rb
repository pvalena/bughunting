#!/usr/bin/ruby

require "test/unit"
require "bughunting/check/limit"
require "bughunting/check/exec"
require "tempfile"

class TestCheckExec< Test::Unit::TestCase
  def setup
  end

  def teardown
  end

  def test_run
    limits = Limit.new
    limits.memory = 30
    exec = Exec.new("bughunting1", "/tmp/foo", limits, $stdout)

    x = exec.run './test.sh "a b c" d'
    p x
  end
end
